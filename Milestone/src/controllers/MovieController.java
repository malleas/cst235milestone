package controllers;

import java.util.ArrayList;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import beans.Movies;
import beans.Product;

/**
 * 
 * @author Matt Sievers
 * @version 1.0
 * Movie controller used for basic MainPage functionality and some movie related calls.
 *
 */

@Named
@ApplicationScoped
public class MovieController {

	public MovieController() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @deprecated
	 * Moved to DAOController for DB calls, can still be used to create a local list of movies.
	 * @param movie
	 * @return
	 */
	public String addMovie(Product movie) {
		FacesContext context = FacesContext.getCurrentInstance();
		Movies movies = context.getApplication().evaluateExpressionGet(context, "#{movies}", Movies.class);
		
		ArrayList<Product> movieList = movies.getMovies();
		
		if(movieList == null) {
			movieList = new ArrayList<Product>();
			System.out.println("I have created a new movie list");
		}
		
		System.out.println("Movie location is: " + movie.getImgLocation());
		movies.addMovie(movie);
		return "MainPage.xhtml?faces-redirect=true";
	}
	
	public void logout() {
		System.out.println("This is before the session is invalidated");
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().invalidateSession();
		try {
			System.out.println("Hello from the try block");
			context.getExternalContext().redirect("LoginForm.xhtml");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @deprecated
	 * Will be moved to DOAController for DB calls to persist data
	 * @param movie
	 */
	public void editMovie(Product movie) {
		//stubbed out for later use
		System.out.println("Congrats, you've used a method that does nothing yet!!!");
	}
	
	/**
	 * @deprecated
	 * Will be moved to DOAController for DB calls to persist data
	 * @param movie
	 */
	public void removeMovie(Product movie) {
		// check if movie exists in list and then remove it.
		// stubbed out for now as not required for MS3
		System.out.println("Congrats, you've used a method that does nothing yet!!!");
	}
	

}
