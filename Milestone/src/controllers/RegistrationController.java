package controllers;

import java.util.ArrayList;
import java.util.Random;

import javax.ejb.Stateless;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import javax.inject.Named;

import beans.User;
import beans.Users;

/**
 * 
 * @author Matt Sievers
 * @version 1.0
 * Used in early development to create a local list of users to display on the MainPage.xhtml.
 * Needs reworked to update the UserService.onsubmit to call /rest/users/user to create a new user.
 *
 */

@Named
@Stateless
public class RegistrationController {
	

	public RegistrationController() {
		// TODO Auto-generated constructor stub
	}
	
	
/**
 * @deprecated
 * @param user
 * @return
 */
	public static String onSubmit(User user) {
		FacesContext context = FacesContext.getCurrentInstance();
		Users users = context.getApplication().evaluateExpressionGet(context, "#{users}", Users.class);
		
		ArrayList<User> userList = users.getUsers();
		if(userList == null) {
			userList = new ArrayList<User>();
			System.out.println("I Created a new list!");
		}
		Random rand = new Random();
		user.setRole(rand.nextInt(3));
		
		System.out.println("adding user: "+user.toString());
		users.addUser(user);
		return "MainPage.xhtml?faces-redirect=true";
	}



}
