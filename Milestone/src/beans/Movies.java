package beans;

import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 * 
 * @author Matt Sievers
 * @version 1.0
 * Bean used to create a list of users.  this was used before the DB linking was established.
 * Still used for UpdateUserPage and requires refactoring once DB would be fully implemented.
 *
 */

@Named
@ApplicationScoped
public class Movies {
	
	private ArrayList<Product> movies = new ArrayList<Product>();

	public Movies() {
		// TODO Auto-generated constructor stub
	}

	public Movies(ArrayList<Product> movies) {
		super();
		this.movies = movies;
	}

	public ArrayList<Product> getMovies() {
		return movies;
	}

	public void setMovies(ArrayList<Product> movies) {
		this.movies = movies;
	}
/**
 * Adds movies to the movie list
 * @param movie
 */
    public void addMovie(Product movie) {
    	this.movies.add(movie);
    }

}
