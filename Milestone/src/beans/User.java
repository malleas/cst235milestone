package beans;

import java.security.Principal;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * @author Matt Sievers
 * @version 1.0
 * User info used for registration and persistance to DB
 *
 */

@Named
@RequestScoped
public class User {

	private int userId;
	private int role;
	@NotNull
	@Size(min = 1, message = "First Name must be more than 1 letter long")
	private String firstName;
	@NotNull
	@Size(min = 1, message = "last Name must be more than 1 letter long")
	private String lastName;
	@NotNull
	@Size(min = 10, message = "Please provide full 10 digit phone number")
	private String phoneNumber;
	@NotNull
	@Size(min = 1, message = "Address must be more than 1 letter long")
	private String streetAddress;
	@NotNull
	@Size(min = 1, message = "City must be more than 1 letter long")
	private String city;
	@NotNull
	@Size(min = 2, max = 2, message = "Please provide state abbreviation only")
	private String state;
	private int zipCode;
	@NotNull
	@Size(min = 1, message = "Username must be more than 1 letter long")
	private String userName;
	@NotNull
	@Size(min = 1, message = "Password must be more than 1 letter long")
	private String password;
	private String confirmPass;

	private boolean editable;
	
	@PostConstruct
	public void init() {
		Principal principal = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();
		if(principal == null) {
			setFirstName("Unknown");
			setLastName("Unknown");
		}else {
			setFirstName(principal.getName());
			setLastName("");
		}
	}

	public User() {
	}

	public User(int userId, int role, String firstName, String lastName, String phoneNumber, String streetAddress, String city,
			String state, int zipCode, String userName, String password, String confirmPass) {
		super();
		this.userId = userId;
		this.role = role;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.streetAddress = streetAddress;
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
		this.userName = userName;
		this.password = password;
		this.confirmPass = confirmPass;
	}
	
	public String editAction(User user) {
		user.setEditable(true);
		return null;
	}
	

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getZipCode() {
		return zipCode;
	}

	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPass() {
		return confirmPass;
	}

	public void setConfirmPass(String confirmPass) {
		this.confirmPass = confirmPass;
	}


	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public boolean isEditable() {
		return editable;
	}
	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", role=" + role + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", phoneNumber=" + phoneNumber + ", streetAddress=" + streetAddress + ", city=" + city + ", state="
				+ state + ", zipCode=" + zipCode + ", userName=" + userName + ", password=" + password
				+ ", confirmPass=" + confirmPass + ", editable=" + editable + "]";
	}
	
	

}
