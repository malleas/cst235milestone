package beans;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * @author Matt Sievers
 * @version 1.0
 * Product was created with Movie having issues and not importing properly.
 * @Product should be used for any reference to a single @Movie
 *
 */

@Named
@ApplicationScoped
public class Product {
	
	@NotNull
	@Size(min = 1, message = "Title must be at least 1 letter long")
	private String title;
	//Need to set a default image should one not be picked. (possible improvement)
	private String imgLocation;
	private int year;
	@NotNull
	@Size(min = 1, message = "Genre must be at least 1 letter long")
	private String genre;
	@NotNull
	@Size(min = 1, message = "Movie Rating must be at least 1 letter long")
	private String movieRating;
	@NotNull
	@Size(min = 1, message = "Studio must be at least 1 letter long")
	private String studio;
	@NotNull
	@Size(min = 1, message = "Director must be at least 1 letter long")
	private String director;
	private double movieLength;
	private double rentalRate;
	private int inventoryCount;


	
	
	public Product(String title, String imgLocation, int year, String genre, String movieRating, String studio,
			String director, double movieLength, double rentalRate, int inventoryCount) {
		super();
		this.title = title;
		this.imgLocation = imgLocation;
		this.year = year;
		this.genre = genre;
		this.movieRating = movieRating;
		this.studio = studio;
		this.director = director;
		this.movieLength = movieLength;
		this.rentalRate = rentalRate;
		this.inventoryCount = inventoryCount;
	}




	public Product() {
		// TODO Auto-generated constructor stub
	}




	public String getTitle() {
		return title;
	}




	public void setTitle(String title) {
		this.title = title;
	}




	public String getImgLocation() {
		return imgLocation;
	}




	public void setImgLocation(String imgLocation) {
		this.imgLocation = imgLocation;
	}




	public int getYear() {
		return year;
	}




	public void setYear(int year) {
		this.year = year;
	}




	public String getGenre() {
		return genre;
	}




	public void setGenre(String genre) {
		this.genre = genre;
	}




	public String getMovieRating() {
		return movieRating;
	}




	public void setMovieRating(String movieRating) {
		this.movieRating = movieRating;
	}




	public String getStudio() {
		return studio;
	}




	public void setStudio(String studio) {
		this.studio = studio;
	}




	public String getDirector() {
		return director;
	}




	public void setDirector(String director) {
		this.director = director;
	}




	public double getMovieLength() {
		return movieLength;
	}




	public void setMovieLength(double movieLength) {
		this.movieLength = movieLength;
	}




	public double getRentalRate() {
		return rentalRate;
	}




	public void setRentalRate(double rentalRate) {
		this.rentalRate = rentalRate;
	}




	public int getInventoryCount() {
		return inventoryCount;
	}




	public void setInventoryCount(int inventoryCount) {
		this.inventoryCount = inventoryCount;
	}

	
	
}
