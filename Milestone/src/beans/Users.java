package beans;

import java.util.ArrayList;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 * 
 * @author Matt Sievers
 * @version 1.0
 * Similar to Movies, was used in early development to display a user within a datatable.  
 * More rework needed as there are still some dependencies to this.
 *
 */

@Named
@ApplicationScoped
public class Users {

	private ArrayList<User> users = new ArrayList<User>();

	public Users(ArrayList<User> users) {
		super();
		this.users = (ArrayList<User>) users;
	}

	public Users() {
		// TODO Auto-generated constructor stub
	}

	public ArrayList<User> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}

	public void addUser(User user) {
		this.users.add(user);
	}
	

	/**
	 * 
	 * @return null
	 */
	public String saveAction() {
		for (User user : users) {
			user.setEditable(false);
		}
		return null;
	}
	
	/**
	 * 
	 * @param user
	 */

	public void removeUser(User user) {
    	this.users.remove(user);

    }
}
