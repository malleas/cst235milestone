package services;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import beans.User;
import dao.UserDataService;

/**
 * 
 * @author Matt Sievers
 * @version 1.0
 *
 */

@RequestScoped
@Path("/users")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class UserService {
	
	@Inject UserDataService userDataService;
	
	/**
	 * 
	 * @param inputFirstName
	 * @param inputLastName
	 * @return
	 * @throws Exception
	 */
	@GET
	@Path("/user")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser(
			@QueryParam("firstName") String inputFirstName,
			@QueryParam("lastName") String inputLastName ) throws Exception {
		
		return userDataService.getUser(inputFirstName, inputLastName);
		
	}
	
	/**
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@POST
	@Path("/user")
	@Consumes("application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addUser(User user) throws Exception {
		return userDataService.addUser(user);
	}

}
