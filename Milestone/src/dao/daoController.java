package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import beans.Product;
import beans.User;
import beans.Users;

/**
 * 
 * @author Matt Sievers
 * @version 1.0 Common helper methods for DB calls used in UpdateUser and
 *          Getting movies for main page. Some duplication is here and needs
 *          reworked between it and UserDataService. This was early in the
 *          development cycle, DataService came later.
 *
 */

@Named
@RequestScoped
public class daoController {

	public daoController() {
		// TODO Auto-generated constructor stub
	}

	// JDBC Driver name and database URL
	static final String JDBC_DRIVER = "org.postgresql.Driver";
	static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";

	// DB Credentials
	static final String USER = "postgres";
	static final String PASS = "password1";
	// Setting Conn settings to null for all methods to use.
	private Connection conn = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	private PreparedStatement ps = null;

	/**
	 * Retrieves all users in DB and stores them to a list for UpdateUserPage.xhtml
	 * @throws Exception
	 */
	public void getAllUsers() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		Users allUsersLlist = context.getApplication().evaluateExpressionGet(context, "#{users}", Users.class);
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);

		try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			String sql = "Select * from milestoneapp.baseuser";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				int userId = rs.getInt("userId");
				int role = rs.getInt("role");
				String firstName = rs.getString("firstName");
				String lastName = rs.getString("lastName");
				String phoneNumber = rs.getString("phoneNumber");
				String streetAddress = rs.getString("streetAddress");
				String city = rs.getString("city");
				String state = rs.getString("state");
				int zipCode = rs.getInt("zipCode");
				String userName = rs.getString("userName");
				String password = rs.getString("password");
				String confirmPassword = rs.getString("confirmPassword");

				user = new User(userId, role, firstName, lastName, phoneNumber, streetAddress, city, state, zipCode,
						userName, password, confirmPassword);
				allUsersLlist.addUser(user);

			}

			stmt.close();
			rs.close();

		} catch (Exception e) {
			System.out.println("Failure: " + e);
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}

	/**
	 * Finds a user by firstName only.  This method is currently not used as it was decided that we will just retrieve all
	 * users on one page and perform actions on them there (update, delete, edit).
	 * @param firstName
	 * @throws Exception
	 */
	public void findUserByFirstName(String firstName) throws Exception {

		try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connection Successful...");

			String sql = "Select * from milestoneapp.baseuser where firstName = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, firstName);
			rs = ps.executeQuery();

			while (rs.next()) {
				int resultUserId = rs.getInt("userId");
				String resultFirstName = rs.getString("firstName");
				String resultLastName = rs.getString("lastName");
				System.out.println("ID: " + resultUserId);
				System.out.println("First Name: " + resultFirstName);
				System.out.println("Last Name: " + resultLastName);
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			System.out.println("Failures: " + e);
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.close();
			}
		}

	}
/**
 * Same as finduserByFirstName
 * @param id
 * @throws Exception
 */
	public void findUserById(int id) throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		Users users = context.getApplication().evaluateExpressionGet(context, "#{users}", Users.class);
		User resultUser = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connection Successful...");

			String sql = "Select * from milestoneapp.baseuser where userId = ?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();

			while (rs.next()) {

				int resultUserId = rs.getInt("userId");
				int resultRole = rs.getInt("role");
				String resultFirstName = rs.getString("firstName");
				String resultLastName = rs.getString("lastName");
				String resultPhoneNumber = rs.getString("phoneNumber");
				String resultStreetAddress = rs.getString("streetAddress");
				String resultCity = rs.getString("city");
				String resultState = rs.getString("state");
				int resultZipCode = rs.getInt("zipCode");
				String resultUserName = rs.getString("userName");
				String resultPassword = rs.getString("password");
				String resultConfirmPassword = rs.getString("confirmPassword");
				System.out.println("ID: " + resultUserId);
				System.out.println("First Name: " + resultFirstName);
				System.out.println("Last Name: " + resultLastName);
				resultUser = new User(resultUserId, resultRole, resultFirstName, resultLastName, resultPhoneNumber,
						resultStreetAddress, resultCity, resultState, resultZipCode, resultUserName, resultPassword,
						resultConfirmPassword);
				users.addUser(resultUser);
			}

			rs.close();
			ps.close();

		} catch (Exception e) {
			System.out.println("Failures: " + e);
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}
/**
 * Used in UpdateUserPage.xhtml.  Takes any updates from the form and persists it to the DB to update user info.
 * @throws Exception
 */
	public void updateUser() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		Users userList = context.getApplication().evaluateExpressionGet(context, "#{users}", Users.class);
		User updatedUser = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);

		System.out.println(userList.getUsers().get(0).getLastName());
		System.out.println(userList.getUsers().get(0).getPhoneNumber());

		System.out.println(updatedUser.toString());

		int userId = userList.getUsers().get(0).getUserId();
		int updatedRole = userList.getUsers().get(0).getRole();
		String updatedFirstName = userList.getUsers().get(0).getFirstName();
		String updatedLastName = userList.getUsers().get(0).getLastName();
		String updatedPhoneNumber = userList.getUsers().get(0).getPhoneNumber();
		String updatedStreetAddress = userList.getUsers().get(0).getStreetAddress();
		String updatedCity = userList.getUsers().get(0).getCity();
		String updatedState = userList.getUsers().get(0).getState();
		int updatedZipCode = userList.getUsers().get(0).getZipCode();
		String updatedUserName = userList.getUsers().get(0).getUserName();
		String updatedPassword = userList.getUsers().get(0).getPassword();
		String updatedConfirmPassword = userList.getUsers().get(0).getConfirmPass();

		System.out.println(userId);
		System.out.println(updatedRole);
		System.out.println(updatedFirstName);
		System.out.println(updatedLastName);
		System.out.println(updatedPhoneNumber);
		System.out.println(updatedStreetAddress);
		System.out.println(updatedCity);
		System.out.println(updatedState);
		System.out.println(updatedZipCode);
		System.out.println(updatedUserName);

		try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connection Successful...");
			String sql = "UPDATE milestoneapp.baseuser SET role = ?, firstName = ?, lastName = ?, phoneNumber = ?, streetAddress = ?, city = ?, state = ?, zipCode = ?, userName = ? WHERE userId = ?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, updatedRole);
			ps.setString(2, updatedFirstName);
			ps.setString(3, updatedLastName);
			ps.setString(4, updatedPhoneNumber);
			ps.setString(5, updatedStreetAddress);
			ps.setString(6, updatedCity);
			ps.setString(7, updatedState);
			ps.setInt(8, updatedZipCode);
			ps.setString(9, updatedUserName);
			ps.setInt(10, userId);
			System.out.println(sql);
			ps.executeUpdate();
			System.out.println("Updated complete...");
			ps.close();

		} catch (Exception e) {
			System.out.println("Failure: " + e);
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}
	
	/**
	 * Deletes a user, used in the UpdateUserPage.xhtml.  Currently works off the build list from getAllUsers
	 * @param user
	 * @throws Exception
	 */

	public void deleteUser(User user) throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		Users userList = context.getApplication().evaluateExpressionGet(context, "#{users}", Users.class);
		int userId = user.getUserId();
		userList.removeUser(user);
		try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connection Successful...");
			String sql = "DELETE FROM milestoneapp.baseuser WHERE userId = ?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, userId);
			ps.executeUpdate();
			System.out.println("Record Deleted Successfully...");
			ps.close();

		} catch (Exception e) {
			System.out.println("Failure: " + e);
			e.fillInStackTrace();
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}

	public Product getMovieInfo(String imgLocation) throws Exception {
		FacesContext.getCurrentInstance().getExternalContext().redirect("MovieInfoPage.xhtml");
		Product movie = null;
		System.out.println("fo" + imgLocation);

		try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connection Successfull...");
			String sql = "Select * From milestoneapp.movie where imgLocation = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, imgLocation);
			rs = ps.executeQuery();
			while (rs.next()) {
				String title = rs.getString("title");
				String img = rs.getString("imgLocation");
				int year = rs.getInt("year");
				String genre = rs.getString("genre");
				String movieRating = rs.getString("movieRating");
				String studio = rs.getString("studio");
				String director = rs.getString("director");
				double movieLength = rs.getDouble("movieLength");
				double rentalRate = rs.getDouble("rentalRage");
				int inventoryCount = rs.getInt("inventoryCount");
				movie = new Product(title, img, year, genre, movieRating, studio, director, movieLength, rentalRate,
						inventoryCount);
				System.out.println(title + " " + movieLength);

			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			System.out.println("Failure: " + e);
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.close();
			}
		}

		return movie;

	}

	/**
	 * 
	 * @return img locations to display on MainPage.xhtml.  These images correspond and are unique to each movie, there can only be one in the
	 * DB per movie.
	 * @throws Exception
	 */
	public ArrayList<String> getAllMovies() throws Exception {
		ArrayList<String> imgLocations = new ArrayList<>();
		try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			String sql = "Select * from milestoneapp.movie";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				String imgLocation = rs.getString("imgLocation");
				imgLocations.add(imgLocation);
			}
			rs.close();
			stmt.close();

		} catch (Exception e) {
			System.out.print("Failure: " + e);
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		return imgLocations;

	}

}
