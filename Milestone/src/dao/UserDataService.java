package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import com.fasterxml.jackson.databind.ObjectMapper;
import beans.User;

/**
 * 
 * @author Matt Sievers
 * @version 1.0 Persistence helper methods for the UserService API.
 */

@Named
@ApplicationScoped
public class UserDataService {

	public UserDataService() {
		// TODO Auto-generated constructor stub
	}

	// JDBC Driver name and database URL
	static final String JDBC_DRIVER = "org.postgresql.Driver";
	static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";

	// DB Credentials
	static final String USER = "postgres";
	static final String PASS = "password1";
	// Setting all connections to null for all methods.
	private Connection conn = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	private PreparedStatement ps = null;

	/**
	 * Called by UserService.getUser
	 * 
	 * @param firstName
	 * @param lastName
	 * @return Response if successful with a message of that user info in a JSON
	 * @throws Exception
	 */

	public Response getUser(String firstName, String lastName) throws Exception {
		String message = null;

		try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connection Successful...");
			String sql = "Select * from milestoneapp.baseuser where firstname = ? and lastname = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, firstName.toLowerCase());
			ps.setString(2, lastName.toLowerCase());
			rs = ps.executeQuery();
			User user = new User();
			while (rs.next()) {
				user.setUserId(rs.getInt("userId"));
				user.setRole(rs.getInt("role"));
				user.setFirstName(rs.getString("firstName"));
				user.setLastName(rs.getString("lastName"));
				user.setPhoneNumber(rs.getString("phoneNumber"));
				user.setStreetAddress(rs.getString("streetAddress"));
				user.setCity(rs.getString("city"));
				user.setState(rs.getString("state"));
				user.setZipCode(rs.getInt("zipCode"));
				user.setUserName(rs.getString("userName"));
				user.setPassword(rs.getString("password"));
				user.setConfirmPass(rs.getString("confirmPassword"));
				ObjectMapper mapper = new ObjectMapper();
				message = mapper.writeValueAsString(user);

			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.close();
			}
		}

		return Response.status(200).entity(message).build();

	}

	/**
	 * Used by UserService.addUser
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */

	public Response addUser(User user) throws Exception {
		String message = null;

		System.out.println(user.getFirstName());
		System.out.println(user.getLastName());

		try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connection Successful...");
			String sql = "INSERT INTO milestoneapp.baseuser(firstname, lastname, phonenumber, streetaddress, city, state, zipcode, username, password, confirmpassword) VALUES(?,?,?,?,?,?,?,?,?,?)";

			ps = conn.prepareStatement(sql);
			ps.setString(1, user.getFirstName());
			ps.setString(2, user.getLastName());
			ps.setString(3, user.getPhoneNumber());
			ps.setString(4, user.getStreetAddress());
			ps.setString(5, user.getCity());
			ps.setString(6, user.getState());
			ps.setInt(7, user.getZipCode());
			ps.setString(8, user.getUserName());
			ps.setString(9, user.getPassword());
			ps.setString(10, user.getConfirmPass());
			ps.executeUpdate();
			message = "User " + user.getFirstName() + " " + user.getLastName() + " added";
			System.out.println("User added successfully...");
			ps.close();

			FacesContext.getCurrentInstance().getExternalContext().redirect("MainPage.xhtml");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.close();
			}
		}

		return Response.status(200).entity(message).build();
	}

}
